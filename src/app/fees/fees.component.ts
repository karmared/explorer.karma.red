import { Component, OnInit } from '@angular/core';
import { OPERATIONS } from '../mock-operations';
import { FeesService } from './fees.service';

@Component({
  selector: 'app-fees',
  templateUrl: './fees.component.html',
  styleUrls: ['./fees.component.css'],
  providers: [FeesService]
})
export class FeesComponent implements OnInit {
  operations = OPERATIONS; fees_info = []; fees = []; index = -1; new_fee = [];

  constructor(private feesService: FeesService) { }

  async ngOnInit() {
    await this.feesService.init();
    await this.feesService.getFees();

    this.fees_info = this.fees_info.concat(this.feesService.fees_info);

    let fee_count; let price_per_kbyte;

    console.log(this.fees_info);
    for (let op of this.operations) {
      for (let fee of this.fees_info['0']['current_fees']['parameters']) {
        if (op['id'] === fee['0']) {
          if ('fee' in fee['1']) {
            fee_count = fee['1']['fee'] / 100000;
          } else if ('membership_lifetime_fee' in fee['1']) {
            fee_count = fee['1']['membership_lifetime_fee'] / 100000;
          } else {
            fee_count = 0;
          }
          if ('price_per_kbyte' in fee['1']) {
            price_per_kbyte = fee['1']['price_per_kbyte'] / 100000;
          } else {
            price_per_kbyte = 0;
          }
          this.fees[fee['0']] = {
            id: op['id'],
            name: op['name'],
            color: op['color'],
            fee: fee_count,
            price_per_kbyte: price_per_kbyte
          };
        }
      }
    }

    // this.new_fee = this.fees;

    for (let fee of this.fees) {
      if ((fee['id'] < 1 || fee['id'] > 5) && (fee['id'] < 10 || fee['id'] > 19) && (fee['id'] < 39 || fee['id'] > 45)) {
        this.new_fee = this.new_fee.concat(fee);
      }
    }
  }

}
