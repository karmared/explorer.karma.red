import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BalanceInformationService } from './balance-information.service';

@Component({
  selector: 'app-balance-information',
  templateUrl: './balance-information.component.html',
  styleUrls: ['./balance-information.component.css'],
  providers: [BalanceInformationService]
})
export class BalanceInformationComponent implements OnInit {
  private sub: any; balance_id; user_id; router_id; balance_info = []; json_string;

  constructor(private route: ActivatedRoute, private balanceInfo: BalanceInformationService) { }

  async ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.router_id = params['id'].split('/');
      this.user_id = this.router_id[0];
      this.balance_id = this.router_id[1];
    });

    await this.balanceInfo.init();
    await this.balanceInfo.getAccountBalance(this.user_id, this.balance_id);

    this.balance_info = this.balanceInfo.balance_info;

    this.json_string = JSON.stringify(this.balance_info, null, '\t');
  }

}
