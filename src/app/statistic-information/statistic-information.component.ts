import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatisticInformationService } from './statistic-information.service';

@Component({
  selector: 'app-statistic-information',
  templateUrl: './statistic-information.component.html',
  styleUrls: ['./statistic-information.component.css'],
  providers: [StatisticInformationService]
})
export class StatisticInformationComponent implements OnInit {
  private sub: any;
  statistic_id; account_id = []; statistic_info = []; owner_name = '';

  constructor(private route: ActivatedRoute, private statService: StatisticInformationService) { }

  async ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.statistic_id = params['id'].split('+');
      this.account_id = this.statistic_id['1'];
    });

    await this.statService.init();
    await this.statService.getAccounts(this.account_id);

    this.statistic_info = this.statistic_info.concat(this.statService.account['0']['1']['statistics']);

    await this.statService.getAccounts(this.statistic_info['0']['owner']);
    this.owner_name = this.statService.account['0']['1']['account']['name'];

    let help_amount = []; let balance = ''; let amount = [];
    amount = String(this.statistic_info['0']['lifetime_fees_paid'] / 100000).split('.');
    help_amount = amount['0'].split('').reverse();
    balance = help_amount['0'];
    for (let j = 1; j < help_amount.length; j++) {
      balance = help_amount[j] + balance;
      if ((j + 1) % 3 === 0) {
        balance = ' ' + balance;
      }
    }
    amount['0'] = balance;
    balance = amount.join(',');
    this.statistic_info['0']['lifetime_fees_paid'] = balance;
  }

}
