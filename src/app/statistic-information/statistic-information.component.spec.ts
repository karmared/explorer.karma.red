import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticInformationComponent } from './statistic-information.component';

describe('StatisticInformationComponent', () => {
  let component: StatisticInformationComponent;
  let fixture: ComponentFixture<StatisticInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
