import { Component, OnInit } from '@angular/core';
import { OperationInformationService } from './operation-information.service';
import { ActivatedRoute } from '@angular/router';
import { OPERATIONS } from '../mock-operations';

@Component({
  selector: 'app-operation-information',
  templateUrl: './operation-information.component.html',
  styleUrls: ['./operation-information.component.css'],
  providers: [OperationInformationService]
})
export class OperationInformationComponent implements OnInit {
  private sub: any; operation_id; json_string; router_id; user_id;
  operation_info = []; list_assets = []; amount = false; account_from = ''; account_to = '';

  constructor(private route: ActivatedRoute, private opInfo: OperationInformationService) { }

  operations = OPERATIONS;

  async ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.router_id = params['id'].split('/');
      this.user_id = this.router_id[0];
      this.operation_id = this.router_id[1];
    });

    await this.opInfo.init();
    await this.opInfo.getAccountHistory(this.user_id, this.operation_id);

    this.operation_info = this.opInfo.operation_info;

    if ('amount' in this.operation_info['op']['1']) {
      this.amount = true;
    }

    for (let op of this.operations) {
      if (op['id'] === this.operation_info['op']['0']) {
        this.operation_info['op']['operation_name'] = op['name'];
        this.operation_info['op']['operation_color'] = op['color'];
      }
    }

    await this.opInfo.getAccountById(this.operation_info['op']['1']['from']);
    this.account_from = this.opInfo.account[0]['name'];

    await this.opInfo.getAccountById(this.operation_info['op']['1']['to']);
    this.account_to = this.opInfo.account[0]['name'];

    // console.log(this.operation_info);
  }

}
