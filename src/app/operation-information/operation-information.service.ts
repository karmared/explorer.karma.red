import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class OperationInformationService {
  dbApi = 0;
  operation_info = []; list_assets = []; account = [];

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  async getAssets() {
    this.list_assets = [];

    await Apis.instance().db_api().exec('list_assets',  ['', 10]).then((result) => {
      let symbol = [];
      for (let i = 0; i < result.length; i++) {
        if (result[i].symbol !== 'KRM' && result[i].symbol !== 'KARMA' && result[i].symbol !== 'KARMA.RUB') {
          symbol = result[i].symbol.split('K');
          result[i].symbol = symbol[1];
        }
        this.list_assets[i] = {
          id: result[i].id,
          name: result[i].symbol,
          precision: result[i].precision
        };
      }
    });
  }

  async getAccountById(id) {
    this.account = [];
    await Apis.instance().db_api().exec('get_accounts', [[id]]).then((result) => {
      this.account = this.account.concat(result);
    });
  }

  balance_refresh(account_balance, balance_precision) {
    let help_amount = []; let balance = ''; let amount = [];
    amount = String(account_balance / Math.pow(10, balance_precision)).split('.');
    help_amount = amount['0'].split('').reverse();
    balance = help_amount['0'];
    for (let j = 1; j < help_amount.length; j++) {
      balance = help_amount[j] + balance;
      if ((j + 1) % 3 === 0) {
        balance = ' ' + balance;
      }
    }
    amount['0'] = balance;
    balance = amount.join(',');
    return account_balance = balance;
  }

  async getAccountHistory(user_id, operation_id) {
    let op; let operation_id_last;
    op = operation_id.split('.');
    op[2] = Number(op[2]);
    operation_id_last = op.join('.');
    op[2] = Number(op[2]) - 1;
    operation_id = op.join('.');

    await this.getAssets();
    await Apis.instance().history_api().exec('get_account_history', [user_id, operation_id, 100, operation_id_last]).then(async (result) => {
      this.operation_info = result[0];
      if ('amount' in this.operation_info['op']['1']) {
        for (let asset of this.list_assets) {
          if (this.operation_info['op']['1']['amount']['asset_id'] === asset['id']) {
            this.operation_info['op']['1']['amount']['asset_name'] = asset['name'];
            this.operation_info['op']['1']['amount']['amount'] = this.balance_refresh(this.operation_info['op']['1']['amount']['amount'], asset['precision']);
          }
          if (this.operation_info['op']['1']['fee']['asset_id'] === asset['id']) {
            this.operation_info['op']['1']['fee']['asset_name'] = asset['name'];
            this.operation_info['op']['1']['fee']['amount'] = this.balance_refresh(this.operation_info['op']['1']['fee']['amount'], asset['precision']);
          }
        }
      }
    });
  }
}
