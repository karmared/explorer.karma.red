import { Component, OnInit } from '@angular/core';
import { CommitteeService} from './committee.service';

@Component({
  selector: 'app-committee',
  templateUrl: './committee.component.html',
  styleUrls: ['./committee.component.css'],
  providers: [CommitteeService]
})
export class CommitteeComponent implements OnInit {
  // active_witnesses = []; active_witnesses_info = []; index = 0; all_witnesses_info = [];
  // standby_witnesses_info = []; all_witnesses = []; standby_witnesses = [];

  all_committee = []; active_committee = []; committee_info = [];
  active_committee_info = []; standby_committee_info = [];


  glyphicon_miss_active = 'glyphicon-sort'; sort_miss_active = true; sort_miss_standby = true; glyphicon_miss_standby= 'glyphicon-sort';
  glyphicon_total_active = 'glyphicon-sort'; sort_total_active = true; sort_total_standby = true; glyphicon_total_standby= 'glyphicon-sort';
  index_2 = 2;
  constructor(private committeeService: CommitteeService) { }

  sort_by_total_active() {
    console.log(this.active_committee_info);
    this.active_committee_info.sort(function (a, b) {
      if (Number(a[3]) > Number(b[3])) {
        return 1;
      }
      if (Number(a[3]) < Number(b[3])) {
        return -1;
      }
      return 0;
    });
    if (this.sort_total_active) {
      this.sort_total_active = false;
      this.glyphicon_total_active = 'glyphicon-sort-by-attributes';
    }else if (!this.sort_total_active) {
      this.sort_total_active = true;
      this.glyphicon_total_active = 'glyphicon-sort-by-attributes-alt';
      this.active_committee_info.reverse();
    }
  }

  sort_by_total_standby() {
    this.standby_committee_info.sort(function (a, b) {
      if (Number(a[6]) > Number(b[6])) {
        return 1;
      }
      if (Number(a[6]) < Number(b[6])) {
        return -1;
      }
      return 0;
    });
    if (this.sort_total_standby) {
      this.sort_total_standby = false;
      this.glyphicon_total_standby = 'glyphicon-sort-by-attributes';
    }else if (!this.sort_total_standby) {
      this.sort_total_standby = true;
      this.glyphicon_total_standby = 'glyphicon-sort-by-attributes-alt';
      this.standby_committee_info.reverse();
    }
  }

  sort(committee_info) {
    let sortable = [];

    for (let committee of committee_info) {
      sortable.push([committee['id'], committee['committee_member_account'], committee['committee_member_account_name'],
        committee['total_votes'], committee['url']]);
    }

    sortable.sort(function(a, b) {
      return a[3] - b[3];
    });

    sortable = sortable.reverse();

    for (let i = 0; i < sortable.length; i++) {
      let help_amount = []; let balance = ''; let amount = [];
      amount = String(sortable[i][3] / 100000).split('.');
      help_amount = amount['0'].split('').reverse();
      balance = help_amount['0'];
      for (let j = 1; j < help_amount.length; j++) {
        balance = help_amount[j] + balance;
        if ((j + 1) % 3 === 0) {
          balance = ' ' + balance;
        }
      }
      amount['0'] = balance;
      balance = amount.join(',');
      sortable[i][3] = balance;
    }

    return committee_info = sortable;
  }

  openURL(url) {
    window.open(url, '_blank');
  }

  async ngOnInit() {
    await this.committeeService.init();
    await this.committeeService.getActiveCommittee();

    await this.committeeService.getAllCommittee();

    await this.committeeService.getCommitteeInfo();
    await this.committeeService.getAccounts();

    this.active_committee = this.committeeService.active_committee;
    this.all_committee = this.committeeService.all_committee;
    this.committee_info = this.committeeService.committee_info;

    for (let committee of this.committee_info) {
      if (this.active_committee.includes(committee['id'])) {
        this.active_committee_info = this.active_committee_info.concat(committee);
      } else {
        this.standby_committee_info = this.standby_committee_info.concat(committee);
      }
    }

    this.active_committee_info = await this.sort(this.active_committee_info);
    console.log(this.active_committee_info);
    this.standby_committee_info = await this.sort(this.standby_committee_info);





    // await this.committeeService.init();
    //
    // await this.committeeService.getActiveWitnesses();
    // this.active_witnesses = this.active_witnesses.concat(this.committeeService.active_witnesses);
    //
    // await this.committeeService.getAllWitness();
    // this.all_witnesses = this.all_witnesses.concat(this.committeeService.all_witnesses);
    //
    // for (let i = 0; i < this.all_witnesses.length; i++) {
    //   if (this.active_witnesses.indexOf(this.all_witnesses[i]) === -1) {
    //     this.standby_witnesses.push(this.all_witnesses[i]);
    //   }
    // }
    //
    // await this.committeeService.getWitnessInfo(this.active_witnesses);
    // this.active_witnesses_info = await this.witnesses_balance(this.sort(this.active_witnesses_info.concat(this.committeeService.witnesses_info)));
    //
    // this.index_2 = this.active_witnesses.length;
    //
    // await this.committeeService.getWitnessInfo(this.standby_witnesses);
    // this.standby_witnesses_info = await this.witnesses_balance(this.sort(this.standby_witnesses_info.concat(this.committeeService.witnesses_info)));
  }

}
