import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class CommitteeService {
  dbApi = 0; all_committee = []; active_committee = []; committee_info = [];

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  // Get active witness
  async getActiveCommittee() {
    this.active_committee = [];
    await Apis.instance().db_api().exec('get_global_properties', []).then(async (result) => {
      this.active_committee = this.active_committee.concat(result['active_committee_members']);
    });
  }

  async getAllCommittee() {
    await Apis.instance().db_api().exec('lookup_committee_member_accounts', ['', 100]).then(async (result) => {
      for (let i = 0; i < result.length; i++) {
        this.all_committee.push(result[i]['1']);
      }
    });
  }

  async getCommitteeInfo() {
    this.committee_info = [];
    for (let i = 0; i < this.all_committee.length; i++) {
      await Apis.instance().db_api().exec('get_committee_members', [[this.all_committee[i]]]).then(async (result) => {
        this.committee_info = this.committee_info.concat(result);
      });
    }
  }

  async getAccounts() {
    for (let i = 0; i < this.committee_info.length; i++) {
      await Apis.instance().db_api().exec('get_full_accounts', [[this.committee_info[i]['committee_member_account']], true]).then((result) => {
        this.committee_info[i]['committee_member_account_name'] = result['0']['1']['account']['name'];
      });
    }
  }
}
