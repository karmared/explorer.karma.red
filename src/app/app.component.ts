import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent implements OnInit {
  title = 'app'; navigation_left = 'navigation-left'; click_nav = false; top_navigation = 'top-navigation-1';
  click_nav_mobile = false;
  main_router_container = 'main-router-container-1';
  accounts = []; account_name; check_var = []; account = [];
  wallet_link = 'http://app.karma.red/?=register';
  border_color_id; border_color_block; block_number; block = []; footer = 'footer-1'; selected_market = 'buy';

  constructor(private appService: AppService, private router: Router) { }

  // onChange() {
  //   // console.log(this.selected_market);
  // }

  ngOnInit() {
    if (window.location.origin.match(/testnet/) !== null) {
      this.wallet_link = 'http://testnet-app.karma.red/?=register';
    }
  }

  navigation() {
    if (!this.click_nav) {
      this.click_nav = true;
      this.navigation_left = 'another-navigation-left';
      this.top_navigation = 'top-navigation-2';
      this.main_router_container = 'main-router-container-2';
      this.footer = 'footer-2';
    } else {
      this.click_nav = false;
      this.navigation_left = 'navigation-left';
      this.top_navigation = 'top-navigation-1';
      this.main_router_container = 'main-router-container-1';
      this.footer = 'footer-1';
    }
  }

  navigation_mobile() {
    if (!this.click_nav_mobile) {
      this.click_nav_mobile = true;
    } else {
      this.click_nav_mobile = false;
    }
  }

  click_mobile() {
    this.click_nav_mobile = false;
  }

  clickRudex() {
    window.open('https://market.rudex.org/#/market/RUDEX.KRM_BTS', '_blank');
  }

  clickOpenledger() {
    window.open('https://openledger.io/market/OPEN.KRM_BTS', '_blank');
  }

  onChange() {
    switch (this.selected_market) {
      case 'rudex':
        window.open('https://market.rudex.org/#/market/RUDEX.KRM_BTS', '_blank');
        break;
      case 'openledger':
        window.open('https://market.rudex.org/#/market/OPEN.KRM_USD', '_blank');
        break;
      default:
        // location.href = 'https://market.rudex.org/#/market/RUDEX.KRM_BTS';
        window.open('https://market.rudex.org/#/market/RUDEX.KRM_BTS', '_blank');
    }
  }

  async searchAccount() {
    await this.appService.init();
    this.account = [];
    if (this.account_name !== undefined) {
      this.check_var = this.account_name.split('.');
      if (this.check_var.length === 3) {
        await this.appService.getAccountById(this.account_name);
        this.account = this.account.concat(this.appService.account);
        if (this.account['0'] === null) {
          this.border_color_id = '#9A0000';
          this.account_name = '';
        } else {
          this.router.navigate(['/accounts', this.account['0']['id']]);
          this.account_name = '';
        }
      } else {
        await this.appService.getAccountByName(this.account_name);
        this.account = this.account.concat(this.appService.account);
        if (this.account['0'] === null) {
          this.border_color_id = '#9A0000';
          this.account_name = '';
        } else {
          this.router.navigate(['/accounts', this.account['0']['id']]);
          this.account_name = '';
        }
      }
    } else {
      this.border_color_id = '#9A0000';
      this.account_name = '';
    }
  }
}
