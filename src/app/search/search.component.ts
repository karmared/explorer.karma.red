import { Component, OnInit } from '@angular/core';
import { SearchService } from './search.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [SearchService]
})
export class SearchComponent implements OnInit {
  accounts = []; account_name; check_var = []; account = [];
  border_color_id; border_color_block; block_number; block = []; login_error = false; block_error = false;

  constructor(private searchService: SearchService, private router: Router) { }

  async searchAccount() {
    this.account = [];
    if (this.account_name !== undefined) {
      this.check_var = this.account_name.split('.');
      if (this.check_var.length === 3) {
        await this.searchService.getAccountById(this.account_name);
        this.account = this.account.concat(this.searchService.account);
        if (this.account['0'] === null) {
          this.border_color_id = '#9A0000';
          this.login_error = true;
        } else {
          this.router.navigate(['/accounts', this.account['0']['id']]);
          this.login_error = true;
        }
      } else {
        await this.searchService.getAccountByName(this.account_name);
        this.account = this.account.concat(this.searchService.account);
        if (this.account['0'] === null) {
          this.border_color_id = '#9A0000';
          this.login_error = true;
        } else {
          this.router.navigate(['/accounts', this.account['0']['id']]);
        }
      }
    } else {
      this.border_color_id = '#9A0000';
      this.login_error = true;
    }
  }

  error() {
    this.login_error = false;
    this.block_error = false;
    this.border_color_id = '#CCCCCC';
    this.border_color_block = '#CCCCCC';
  }

  async searchBlock() {
    this.block = [];
    if (this.block_number !== undefined) {
      if (isNaN(Number(this.block_number))) {
        this.border_color_block = '#9A0000';
        this.block_error = true;
      } else {
        await this.searchService.getBlock(this.block_number);
        this.block = this.block.concat(this.searchService.block);
        if (this.block['0'] === null) {
          this.border_color_block = '#9A0000';
          this.block_error = true;
        } else {
          this.router.navigate(['/operations/blocks', this.block_number]);
        }
      }
    } else {
      this.border_color_block = '#9A0000';
      this.block_error = true;
    }
  }

  async ngOnInit() {
    await this.searchService.init();
  }

}
