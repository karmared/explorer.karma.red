import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountObjectComponent } from './account-object.component';

describe('AccountObjectComponent', () => {
  let component: AccountObjectComponent;
  let fixture: ComponentFixture<AccountObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
