import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class HomePageService {
  dbApi = 0;
  global_info = []; account_count = 0; witnesses_count = 0; committee_count = 0;

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  async getAccountsCount() {
    await Apis.instance().db_api().exec('get_account_count', []).then((result) => {
      this.account_count = result;
    });
  }

  async getActiveWitnesses() {
    await Apis.instance().db_api().exec('get_global_properties', []).then(async (result) => {
      this.witnesses_count = result['active_witnesses'].length;
    });
  }

  async getCommittee() {
    await Apis.instance().db_api().exec('get_committee_count', []).then(async (result) => {
      this.committee_count = result;
    });
  }

  async getDynamicGlobalProperties() {
    this.global_info = [];
    await Apis.instance().db_api().exec('get_dynamic_global_properties', []).then((result) => {
      this.global_info = this.global_info.concat(result);
    });
  }
}
