import { Component, OnInit } from '@angular/core';
import { HomePageService } from './home-page.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  providers: [HomePageService]
})
export class HomePageComponent implements OnInit {
  global_info = []; date = [];
  global_properties = []; account_count = 0; witnesses_count = 0; committee_count = 0;

  constructor(private homeService: HomePageService) { }

  time(time) {
    this.date = [];
    this.date = time.split('T');
    this.date['0'] = this.date['0'].split('-');
    this.date['0'] = this.date['0'].reverse();
    this.date['0'] = this.date['0'].join('/');
    this.date['0'] = this.date.join(', ');

    return time = this.date['0'];
  }

  async changes() {
    await this.homeService.getDynamicGlobalProperties();

    let global_info = [];
    global_info = global_info.concat(this.homeService.global_info);

    global_info['0']['witness_budget'] = global_info['0']['witness_budget'] / 100000;

    global_info['0']['last_budget_time'] = this.time(global_info['0']['last_budget_time']);
    global_info['0']['next_maintenance_time'] = this.time(global_info['0']['next_maintenance_time']);
    global_info['0']['time'] = this.time(global_info['0']['time']);

    let key_name = []; let key_name_help = []; let new_key = '';

    for (let key in global_info['0']) {
      key_name = []; key_name_help = [];
      key_name = key.split('_');
      key_name_help = key_name[0].split('');
      key_name_help[0] = key_name_help[0].toUpperCase();
      key_name[0] = key_name_help.join('');
      new_key = key_name.join(' ');
    }

    return global_info;
  }

  async ngOnInit() {
    await this.homeService.init();
    await this.homeService.getAccountsCount();
    await this.homeService.getActiveWitnesses();
    await this.homeService.getCommittee();

    this.account_count = this.homeService.account_count;
    this.witnesses_count = this.homeService.witnesses_count;
    this.committee_count = this.homeService.committee_count;

    this.global_properties = await this.changes();
    setInterval(async () => { this.global_properties = await this.changes(); }, 5 * 1000);
  }
}
