import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
  // chain_id: 'c85b4a30545e09c01aaa7943be89e9785481c1e7bd5ee7d176cb2b3d8dd71a70'
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class AccountsService {
  dbApi = 0;
  accounts = []; account_count;

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then((res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  // Get all accounts and ids
  async getAccounts() {
    let amount = []; let help_amount = [];
    let balance; let all_id = []; let all_account = [];
    await Apis.instance().db_api().exec('get_account_count', []).then((result) => {
      this.account_count = result;
    });

    for (let i = 0; i < this.account_count; i++) {
      all_id[i] = '1.2.' + i;
    }

    await Apis.instance().db_api().exec('get_full_accounts', [all_id, true]).then((result) => {
      all_account = all_account.concat(result);
    });

    for (let i = 0; i < this.account_count; i++) {
      if ('0' in all_account[i]['1']['balances']) {
        balance = all_account[i]['1']['balances']['0']['balance'];
      } else {
        balance = 0;
      }
      this.accounts[i] = {
        0: all_account[i]['1']['account']['name'],
        1: all_account[i]['1']['account']['id'],
        2: balance
      };
    }

    let sortable = [];

    for (let i = 0; i < this.accounts.length; i++) {
      await sortable.push([this.accounts[i]['0'], this.accounts[i]['1'], this.accounts[i]['2'], this.accounts[i]['2']]);
    }

    await sortable.sort(function(a, b) {
      return a[2] - b[2];
    });

    sortable = sortable.reverse();
    this.accounts = sortable;

    for (let i = 0; i < this.accounts.length; i++) {
      amount = String(this.accounts[i]['2'] / 100000).split('.');
      help_amount = amount['0'].split('').reverse();
      balance = help_amount['0'];
      for (let j = 1; j < help_amount.length; j++) {
        balance = help_amount[j] + balance;
        if ((j + 1) % 3 === 0) {
          balance = ' ' + balance;
        }
      }
      amount['0'] = balance;
      balance = amount.join(',');
      this.accounts[i]['2'] = balance;
    }
  }
}
