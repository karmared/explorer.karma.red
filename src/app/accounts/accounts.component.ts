import { Component, OnInit } from '@angular/core';
import { AccountsService } from './accounts.service';
import { PagerService } from '../services/pager.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css'],
  providers: [AccountsService, PagerService]
})
export class AccountsComponent implements OnInit {

  accounts = []; pager: any = {}; loading = false;

  // paged items
  pagedItems: any[]; allItems = []; glyphicon_id = 'glyphicon-sort';
  glyphicon_name = 'glyphicon-sort'; glyphicon_amount = 'glyphicon-sort';
  sort_id = true; sort_name = true; sort_amount = true;

  constructor(private accService: AccountsService, private pagerService: PagerService) {}

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.accounts.length, page);

    // get current page of items
    this.pagedItems = this.accounts.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  sort_by_id() {
    this.glyphicon_name = 'glyphicon-sort';
    this.glyphicon_amount = 'glyphicon-sort';
    this.accounts.sort(function (a, b) {
      if (Number(a['1'].split('.')[2]) > Number(b['1'].split('.')[2])) {
        return 1;
      }
      if (Number(a['1'].split('.')[2]) < Number(b['1'].split('.')[2])) {
        return -1;
      }
      return 0;
    });
    if (this.sort_id) {
      this.sort_id = false;
      this.glyphicon_id = 'glyphicon-sort-by-attributes';
      this.setPage(1);
    }else if (!this.sort_id) {
      this.sort_id = true;
      this.glyphicon_id = 'glyphicon-sort-by-attributes-alt';
      this.accounts.reverse();
      this.setPage(1);
    }
  }

  sort_by_name() {
    this.glyphicon_id = 'glyphicon-sort';
    this.glyphicon_amount = 'glyphicon-sort';
    this.accounts.sort(function (a, b) {
      if (a['0'] > b['0']) {
        return 1;
      }
      if (a['0'] < b['0']) {
        return -1;
      }
      return 0;
    });
    if (this.sort_name) {
      this.sort_name = false;
      this.glyphicon_name = 'glyphicon-sort-by-attributes';
      this.setPage(1);
    }else if (!this.sort_name) {
      this.sort_name = true;
      this.glyphicon_name = 'glyphicon-sort-by-attributes-alt';
      this.accounts.reverse();
      this.setPage(1);
    }
  }

  sort_by_amount() {
    this.glyphicon_id = 'glyphicon-sort';
    this.glyphicon_name = 'glyphicon-sort';
    this.accounts.sort(function (a, b) {
      if (Number(a['3']) > Number(b['3'])) {
        return 1;
      }
      if (Number(a['3']) < Number(b['3'])) {
        return -1;
      }
      return 0;
    });
    if (this.sort_amount) {
      this.sort_amount = false;
      this.glyphicon_amount = 'glyphicon-sort-by-attributes';
      this.setPage(1);
    }else if (!this.sort_amount) {
      this.sort_amount = true;
      this.glyphicon_amount = 'glyphicon-sort-by-attributes-alt';
      this.accounts.reverse();
      this.setPage(1);
    }
  }

  async ngOnInit() {
    await this.accService.init();
    await this.accService.getAccounts();

    this.accounts = this.accService.accounts;
    this.loading = true;
    this.setPage(1);
  }

}
