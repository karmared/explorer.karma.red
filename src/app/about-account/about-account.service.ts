import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class AboutAccountService {
  dbApi = 0; all_account = []; account = []; account_balance_KRM;
  about_account = []; list_assets = []; account_balance = []; account_history = []; block_info = [];

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  // Get account by id
  async getAccountById(id) {
    this.about_account = [];

    await Apis.instance().db_api().exec('get_accounts', [[id]]).then(async (result) => {
      // console.log(result);
      this.about_account = result;
    });
  }

  async getAccounts(id) {
    await this.getAssets();

    this.account = [];
    await Apis.instance().db_api().exec('get_full_accounts', [[id], true]).then((result) => {
      // console.log(result);
      this.account = this.account.concat(result);
      if (this.account['0']['1']['vesting_balances'].length !== 0) {
        for (let vest_balance of this.account['0']['1']['vesting_balances']) {
          for (let list of this.list_assets) {
            if (list['id'] === vest_balance['balance']['asset_id']) {
              vest_balance['balance']['asset'] = list['name'];
              vest_balance['balance']['precision'] = list['precision'];
            }
          }
          vest_balance['balance']['amount'] = this.balance_refresh(vest_balance['balance']['amount'], vest_balance['balance']['precision']);
        }
      }
      this.account['0']['1']['statistics']['lifetime_fees_paid'] = this.balance_refresh(this.account['0']['1']['statistics']['lifetime_fees_paid'], 5);
    });

    // console.log(this.account);
  }

  balance_refresh(account_balance, balance_precision) {
    let help_amount = []; let balance = ''; let amount = [];
    amount = String(account_balance / Math.pow(10, balance_precision)).split('.');
    help_amount = amount['0'].split('').reverse();
    balance = help_amount['0'];
    for (let j = 1; j < help_amount.length; j++) {
      balance = help_amount[j] + balance;
      if ((j + 1) % 3 === 0) {
        balance = ' ' + balance;
      }
    }
    amount['0'] = balance;
    balance = amount.join(',');
    return account_balance = balance;
  }

  async getBalance(id) {
    this.account_balance_KRM = 0;
    await Apis.instance().db_api().exec('get_account_balances', [id, ['1.3.0']]).then(async (result) => {
      console.log(result);
      this.account_balance_KRM = result['0']['amount'];
      this.account_balance_KRM = this.balance_refresh(this.account_balance_KRM, 5);
    });
  }

  async getAccountBalance(id) {
    this.account_balance = [];
    let amount = [];
    await this.getAssets();
    await Apis.instance().db_api().exec('get_account_balances', [id, []]).then(async (result) => {
      let symbol; let precision;
      for (let i = 0; i < result.length; i++) {
        for (let j = 0; j < this.list_assets.length; j++) {
          if (result[i].asset_id === this.list_assets[j].id) {
            symbol = this.list_assets[j].name;
            precision = this.list_assets[j].precision;
          }
        }
        this.account_balance[i] = {
          id: result[i].asset_id,
          asset: symbol,
          balance: result[i].amount,
          precision: precision
        };
      }
    });

    for (let i = 0; i < this.account_balance.length; i++) {
      this.account_balance[i]['balance'] = this.balance_refresh(this.account_balance[i]['balance'], this.account_balance[i]['precision']);
    }
  }

  async getAssets() {
    this.list_assets = [];

    await Apis.instance().db_api().exec('list_assets',  ['', 10]).then((result) => {
      let symbol = [];
      for (let i = 0; i < result.length; i++) {
        if (result[i].symbol !== 'KRM' && result[i].symbol !== 'KARMA' && result[i].symbol !== 'KARMA.RUB') {
          symbol = result[i].symbol.split('K');
          result[i].symbol = symbol[1];
        }
        this.list_assets[i] = {
          id: result[i].id,
          name: result[i].symbol,
          precision: result[i].precision
        };
      }
    });
  }

  // Get account by id
  async getAccountHistory(id, total_ops) {
    this.account_history = [];
    let last_id = ''; let start_id = ''; let last_id_active = '';
    if (total_ops < 100) {
      await Apis.instance().history_api().exec('get_account_history', [id, '1.11.0', 100, '1.11.9999999999']).then(async (result) => {
        this.account_history = this.account_history.concat(result);
      });
    } else {
      await Apis.instance().history_api().exec('get_account_history', [id, '1.11.0', 100, '1.11.9999999999']).then(async (result) => {
        last_id_active = result[0]['id'];
      });
      last_id = last_id_active;
      start_id = '1.11.' + (Number(last_id.split('.')[2]) - 100);
      while (this.account_history.length !== total_ops && last_id !== '1.11.0') {
        await Apis.instance().history_api().exec('get_account_history', [id, start_id, 100, last_id]).then(async (result) => {
          if (result.length !== 0) {
            this.account_history = this.account_history.concat(result);
          }
        });
        last_id = start_id;
        if (Number(start_id.split('.')[2]) < 100) {
          start_id = '1.11.0';
        } else {
          start_id = '1.11.' + (Number(start_id.split('.')[2]) - 100);
        }
      }
    }
  }

  async getBlockInfo(block_num) {
    this.block_info = [];

    await Apis.instance().db_api().exec('get_block',  [block_num]).then(async (result) => {
      this.block_info = this.block_info.concat(result);
    });
  }
}
