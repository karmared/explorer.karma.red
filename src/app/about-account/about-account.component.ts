import { Component, OnInit } from '@angular/core';
import { AboutAccountService } from './about-account.service';
import { ActivatedRoute } from '@angular/router';
import { OPERATIONS } from '../mock-operations';
import { PagerService } from '../services/pager.service';

@Component({
  selector: 'app-about-account',
  templateUrl: './about-account.component.html',
  styleUrls: ['./about-account.component.css'],
  providers: [AboutAccountService, PagerService]
})
export class AboutAccountComponent implements OnInit {
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[]; allItems = []; total_ops; about_account = []; account_balance = []; account_history = []; account_statistic = [];
  private sub: any; account_id; date_time = []; account_balance_KRM; account_owner; last_id; account_vest_balance = [];

  loading = false; member = '';

  constructor(private route: ActivatedRoute, private aboutAccService: AboutAccountService, private pagerService: PagerService) {
    this.sub = this.route.params.subscribe(async params => {
      this.last_id = this.account_id;
      this.account_id = params['name'];

      if (this.last_id !== undefined && this.last_id !== this.account_id) {
        await this.ngOnInit();
      }
    });
  }

  operations = OPERATIONS;

  async newID(account_id) {
    this.account_id = account_id;

    this.about_account = []; this.account_balance = []; this.account_history = [];
    this.account_id = ''; this.date_time = [];

    await this.ngOnInit();
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.account_history.length, page);

    // get current page of items
    this.pagedItems = this.account_history.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  async ngOnInit() {
    this.about_account = []; this.account_balance = []; this.account_history = [];
    this.date_time = []; this.account_owner = ''; this.account_statistic = []; this.account_balance_KRM = '';

    await this.aboutAccService.init();
    await this.aboutAccService.getAccountById(this.account_id);
    await this.aboutAccService.getAccounts(this.account_id);

    this.about_account = this.about_account.concat(this.aboutAccService.account['0']['1']['account']);

    if (this.about_account['0']['id'] === this.about_account['0']['lifetime_referrer']) {
      this.member = 'Lifetime Member';
    } else {
      this.member = 'Free member';
    }

    this.account_vest_balance = this.account_vest_balance.concat(this.aboutAccService.account['0']['1']['vesting_balances']);
    console.log(this.account_vest_balance);
    this.account_statistic = this.account_statistic.concat(this.aboutAccService.account['0']['1']['statistics']);

    this.total_ops = this.account_statistic['0']['total_ops'];

    await this.aboutAccService.getAccountById(this.aboutAccService.account['0']['1']['statistics']['owner']);

    this.account_owner = {
      name: this.aboutAccService.about_account['0']['name']
    };

    await this.aboutAccService.getAccountById(this.about_account['0']['referrer']);
    this.about_account['0']['referrer_name'] = this.aboutAccService.about_account['0']['name'];

    await this.aboutAccService.getAccountById(this.about_account['0']['registrar']);
    this.about_account['0']['registrar_name'] = this.aboutAccService.about_account['0']['name'];

    await this.aboutAccService.getAccountBalance(this.account_id);
    await this.aboutAccService.getAssets();

    await this.aboutAccService.getBalance(this.account_id);
    this.account_balance_KRM = this.aboutAccService.account_balance_KRM;

    this.account_balance = this.account_balance.concat(this.aboutAccService.account_balance);

    await this.aboutAccService.getAccountHistory(this.account_id, this.total_ops);
    this.account_history = this.account_history.concat(this.aboutAccService.account_history);

    for (let i = 0; i < this.account_history.length; i++) {
      await this.aboutAccService.getBlockInfo(this.account_history[i]['block_num']);

      this.account_history[i]['block_num'] = {
        block_num: this.account_history[i]['block_num'],
        information: this.aboutAccService.block_info
      };

      this.date_time = this.account_history[i]['block_num']['information']['0']['timestamp'].split('T');
      this.date_time['0'] = this.date_time['0'].split('-');
      this.date_time['0'] = this.date_time['0'].reverse();
      this.date_time['0'] = this.date_time['0'].join('/');
      this.date_time['0'] = this.date_time.join(', ');
      this.account_history[i]['block_num']['information']['0']['timestamp'] = this.date_time['0'];

      for (let op of this.operations) {
        if (this.account_history[i]['op']['0'] === op['id']) {
          this.account_history[i]['op']['operation_name'] = op['name'];
          this.account_history[i]['op']['operation_color'] = op['color'];
        }
      }
    }

    this.loading = true;
    this.setPage(1);
  }

}
