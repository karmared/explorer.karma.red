import { Component, OnInit } from '@angular/core';
import { TransactionService } from './transaction.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css'],
  providers: [TransactionService]
})
export class TransactionsComponent implements OnInit {
  transactions = [];

  constructor(private trService: TransactionService) { }

  async ngOnInit() {
    await this.trService.init();
    await this.trService.getTransactions();

    this.transactions = this.transactions.concat(this.trService.transactions);
  }

}
