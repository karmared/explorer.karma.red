import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class TransactionService {
  dbApi = 0;
  transactions = [];

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  // Get account by id
  async getTransactions() {
    await Apis.instance().db_api().exec('get_dynamic_global_properties', []).then(async (result) => {
      this.transactions = this.transactions.concat(result);
    });

    await Apis.instance().db_api().exec('get_block', [this.transactions['head_block_number']]).then(async (result) => {
      // this.transactions = this.transactions.concat(result);
    });
  }
}
