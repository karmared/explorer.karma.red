import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WitnessInformationComponent } from './witness-information.component';

describe('WitnessInformationComponent', () => {
  let component: WitnessInformationComponent;
  let fixture: ComponentFixture<WitnessInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WitnessInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WitnessInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
