import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WitnessInformationService } from './witness-information.service';

@Component({
  selector: 'app-witness-information',
  templateUrl: './witness-information.component.html',
  styleUrls: ['./witness-information.component.css'],
  providers: [WitnessInformationService]
})
export class WitnessInformationComponent implements OnInit {

  private sub: any;
  witness_id; witness_info = []; account_name = '';

  constructor(private route: ActivatedRoute, private witnessService: WitnessInformationService) { }

  async ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.witness_id = params['id'];
    });

    await this.witnessService.init();
    await this.witnessService.getWitness(this.witness_id);

    this.witness_info = this.witness_info.concat(this.witnessService.witness_info);

    await this.witnessService.getAccountById(this.witness_info['0']['witness_account']);
    this.account_name = this.witnessService.account[0]['name'];

    let help_amount = []; let balance = ''; let amount = [];
    amount = String(this.witness_info['0']['total_votes'] / 100000).split('.');
    help_amount = amount['0'].split('').reverse();
    balance = help_amount['0'];
    for (let j = 1; j < help_amount.length; j++) {
      balance = help_amount[j] + balance;
      if ((j + 1) % 3 === 0) {
        balance = ' ' + balance;
      }
    }
    amount['0'] = balance;
    balance = amount.join(',');
    this.witness_info['0']['total_votes'] = balance;
  }

}
