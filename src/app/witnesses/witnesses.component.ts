import { Component, OnInit } from '@angular/core';
import { WitnessesService } from './witnesses.service';

@Component({
  selector: 'app-witnesses',
  templateUrl: './witnesses.component.html',
  styleUrls: ['./witnesses.component.css'],
  providers: [WitnessesService]
})
export class WitnessesComponent implements OnInit {
  active_witnesses = []; active_witnesses_info = []; index = 0; all_witnesses_info = [];
  standby_witnesses_info = []; all_witnesses = []; standby_witnesses = [];
  glyphicon_miss_active = 'glyphicon-sort'; sort_miss_active = true; sort_miss_standby = true; glyphicon_miss_standby= 'glyphicon-sort';
  glyphicon_total_active = 'glyphicon-sort'; sort_total_active = true; sort_total_standby = true; glyphicon_total_standby= 'glyphicon-sort';
  index_2 = 2;
  constructor(private witnessesService: WitnessesService) { }

  sort_by_miss_active() {
    this.glyphicon_total_active = 'glyphicon-sort';
    this.active_witnesses_info.sort(function (a, b) {
      if (a[4] > b[4]) {
        return 1;
      }
      if (a[4] < b[4]) {
        return -1;
      }
      return 0;
    });
    if (this.sort_miss_active) {
      this.sort_miss_active = false;
      this.glyphicon_miss_active = 'glyphicon-sort-by-attributes';
    }else if (!this.sort_miss_active) {
      this.sort_miss_active = true;
      this.glyphicon_miss_active = 'glyphicon-sort-by-attributes-alt';
      this.active_witnesses_info.reverse();
    }
  }

  sort_by_miss_standby() {
    this.glyphicon_total_standby = 'glyphicon-sort';
    this.standby_witnesses_info.sort(function (a, b) {
      if (a[4] > b[4]) {
        return 1;
      }
      if (a[4] < b[4]) {
        return -1;
      }
      return 0;
    });
    if (this.sort_miss_standby) {
      this.sort_miss_standby = false;
      this.glyphicon_miss_standby = 'glyphicon-sort-by-attributes';
    }else if (!this.sort_miss_standby) {
      this.sort_miss_standby = true;
      this.glyphicon_miss_standby = 'glyphicon-sort-by-attributes-alt';
      this.standby_witnesses_info.reverse();
    }
  }

  sort_by_total_active() {
    this.glyphicon_miss_active = 'glyphicon-sort';
    this.active_witnesses_info.sort(function (a, b) {
      if (Number(a[6]) > Number(b[6])) {
        return 1;
      }
      if (Number(a[6]) < Number(b[6])) {
        return -1;
      }
      return 0;
    });
    if (this.sort_total_active) {
      this.sort_total_active = false;
      this.glyphicon_total_active = 'glyphicon-sort-by-attributes';
    }else if (!this.sort_total_active) {
      this.sort_total_active = true;
      this.glyphicon_total_active = 'glyphicon-sort-by-attributes-alt';
      this.active_witnesses_info.reverse();
    }
  }

  sort_by_total_standby() {
    this.glyphicon_miss_standby = 'glyphicon-sort';
    this.standby_witnesses_info.sort(function (a, b) {
      if (Number(a[6]) > Number(b[6])) {
        return 1;
      }
      if (Number(a[6]) < Number(b[6])) {
        return -1;
      }
      return 0;
    });
    if (this.sort_total_standby) {
      this.sort_total_standby = false;
      this.glyphicon_total_standby = 'glyphicon-sort-by-attributes';
    }else if (!this.sort_total_standby) {
      this.sort_total_standby = true;
      this.glyphicon_total_standby = 'glyphicon-sort-by-attributes-alt';
      this.standby_witnesses_info.reverse();
    }
  }

  sort(witnesses_info) {
    let sortable = [];

    for (let wit of witnesses_info) {
      sortable.push([wit['id'], wit['witness_account'], wit['url'], wit['total_votes'], wit['total_missed'], wit['last_confirmed_block_num'], wit['total_votes']]);
    }

    sortable.sort(function(a, b) {
      return a[3] - b[3];
    });

    sortable = sortable.reverse();
    return witnesses_info = sortable;
  }

  async witnesses_balance(witnesses_info) {
    let i = 1;
    for (let wit of witnesses_info) {
      if (Number(wit[5]) === 0) {
        wit[5] = '';
      }

      let help_amount = []; let balance = ''; let amount = [];
      amount = String(wit['3'] / 100000).split('.');
      help_amount = amount['0'].split('').reverse();
      balance = help_amount['0'];
      for (let j = 1; j < help_amount.length; j++) {
        balance = help_amount[j] + balance;
        if ((j + 1) % 3 === 0) {
          balance = ' ' + balance;
        }
      }
      amount['0'] = balance;
      balance = amount.join(',');
      wit['3'] = balance;

      await this.witnessesService.getAccounts(wit['1']);

      wit['1'] = {
        name: this.witnessesService.account['0']['1']['account']['name'],
        id: this.witnessesService.account['0']['1']['account']['id']
      };
    }

    return witnesses_info;
  }

  openURL(url) {
    window.open(url, '_blank');
  }

  async ngOnInit() {
    await this.witnessesService.init();

    await this.witnessesService.getActiveWitnesses();
    this.active_witnesses = this.active_witnesses.concat(this.witnessesService.active_witnesses);

    await this.witnessesService.getAllWitness();
    this.all_witnesses = this.all_witnesses.concat(this.witnessesService.all_witnesses);

    for (let i = 0; i < this.all_witnesses.length; i++) {
      if (this.active_witnesses.indexOf(this.all_witnesses[i]) === -1) {
        this.standby_witnesses.push(this.all_witnesses[i]);
      }
    }

    await this.witnessesService.getWitnessInfo(this.active_witnesses);
    this.active_witnesses_info = await this.witnesses_balance(this.sort(this.active_witnesses_info.concat(this.witnessesService.witnesses_info)));

    this.index_2 = this.active_witnesses.length;

    await this.witnessesService.getWitnessInfo(this.standby_witnesses);
    this.standby_witnesses_info = await this.witnesses_balance(this.sort(this.standby_witnesses_info.concat(this.witnessesService.witnesses_info)));
  }

}
