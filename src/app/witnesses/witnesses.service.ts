import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class WitnessesService {
  dbApi = 0; active_witnesses = []; account = []; witnesses_info = [];
  all_witnesses = [];

  constructor() {
    this.dbApi = null;
  }

  and = 0;

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  // Get active witness
  async getActiveWitnesses() {
    this.active_witnesses = [];
    await Apis.instance().db_api().exec('get_global_properties', []).then(async (result) => {
      this.active_witnesses = this.active_witnesses.concat(result['active_witnesses']);
    });
  }

  async getAllWitness() {
    await Apis.instance().db_api().exec('lookup_witness_accounts', ['', 100]).then(async (result) => {
      for (let i = 0; i < result.length; i++) {
        this.all_witnesses.push(result[i]['1']);
      }
    });
  }

  async getWitnessInfo(ids) {
    this.witnesses_info = [];
    await Apis.instance().db_api().exec('get_witnesses', [ids]).then(async (result) => {
      this.witnesses_info = this.witnesses_info.concat(result);
    });
  }

  async getAccounts(id) {
    this.account = [];
    await Apis.instance().db_api().exec('get_full_accounts', [[id], true]).then((result) => {
      this.account = this.account.concat(result);
    });
  }
}
