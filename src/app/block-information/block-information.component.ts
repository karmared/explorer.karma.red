import { Component, OnInit } from '@angular/core';
import { BlockInformationService } from './block-information.service';
import { ActivatedRoute } from '@angular/router';
import { OPERATIONS } from '../mock-operations';

@Component({
  selector: 'app-block-information',
  templateUrl: './block-information.component.html',
  styleUrls: ['./block-information.component.css'],
  providers: [BlockInformationService]
})
export class BlockInformationComponent implements OnInit {

  private sub: any; block_num; block_info = [];
  json_string; show_amount = false; date = []; account_from = ''; account_to = '';

  operations = OPERATIONS;

  constructor(private route: ActivatedRoute, private blockInfo: BlockInformationService) { }

  async ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.block_num = params['num'];
    });

    await this.blockInfo.init();
    await this.blockInfo.getBlockInfo(this.block_num);

    this.block_info = this.blockInfo.block_info;
    this.date = [];

    this.date = this.block_info['0']['timestamp'].split('T');
    this.date['0'] = this.date['0'].split('-');
    this.date['0'] = this.date['0'].reverse();
    this.date['0'] = this.date['0'].join('/');
    this.date['0'] = this.date.join(', ');
    this.block_info['0']['timestamp'] = this.date['0'];

    for (let tr of this.block_info['0']['transactions']) {
      if ('amount' in tr['operations']['0']['1']) {
        this.show_amount = true;
      }

      this.date = [];

      this.date = tr['expiration'].split('T');
      this.date['0'] = this.date['0'].split('-');
      this.date['0'] = this.date['0'].reverse();
      this.date['0'] = this.date['0'].join('/');
      this.date['0'] = this.date.join(', ');
      tr['expiration'] = this.date['0'];

      for (let op of this.operations) {
        if (tr['operations']['0']['0'] === op['id']) {
          tr['operations']['0']['operation_name'] = op['name'];
          tr['operations']['0']['operation_color'] = op['color'];
        }
      }
    }

    if (this.block_info['0']['transactions']['0']) {
      await this.blockInfo.getAccountById(this.block_info['0']['transactions']['0']['operations']['0']['1']['from']);
      this.account_from = this.blockInfo.account[0]['name'];
      await this.blockInfo.getAccountById(this.block_info['0']['transactions']['0']['operations']['0']['1']['to']);
      this.account_to = this.blockInfo.account[0]['name'];
    }
  }

}
