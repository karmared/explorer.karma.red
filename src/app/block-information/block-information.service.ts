import { Apis, ChainConfig } from 'karmajs-ws';
import { ChainStore, FetchChain, PrivateKey, key, TransactionHelper, Aes, TransactionBuilder } from 'karmajs';
import { Injectable } from '@angular/core';
import { CHAIN_ID } from '../chain-id';
import { KARMA_IP } from '../karma-ip';

ChainConfig.networks['Karma'] = {
  core_asset: 'KRM',
  address_prefix: 'KRM',
  chain_id: CHAIN_ID
};

let privKey = '5KjYXPnNoxjxUkgENSPMpm6SjrHB1XPV9XxiLzbX8swX8Y3rKT1';
let pKey = PrivateKey.fromWif(privKey);

ChainConfig.setPrefix('KRM');

@Injectable()
export class BlockInformationService {
  dbApi = 0;
  block_info = []; list_assets = []; account = [];

  constructor() {
    this.dbApi = null;
  }

  async init() {
    await Apis.instance(KARMA_IP, true).init_promise.then(async (res) => {
      console.log('connected to:', res[0].network_name, 'network');
    });
  }

  async getAssets() {
    await Apis.instance().db_api().exec('list_assets',  ['', 10]).then((result) => {
      let symbol = [];
      for (let i = 0; i < result.length; i++) {
        if (result[i].symbol !== 'KRM' && result[i].symbol !== 'KARMA' && result[i].symbol !== 'KARMA.RUB') {
          symbol = result[i].symbol.split('K');
          result[i].symbol = symbol[1];
        }
        this.list_assets[i] = {
          id: result[i].id,
          name: result[i].symbol,
          precision: result[i].precision
        };
      }
    });
  }

  async getAccountById(id) {
    this.account = [];
    await Apis.instance().db_api().exec('get_accounts', [[id]]).then((result) => {
      this.account = this.account.concat(result);
    });
  }

  balance_refresh(account_balance, balance_precision) {
    let help_amount = []; let balance = ''; let amount = [];
    amount = String(account_balance / Math.pow(10, balance_precision)).split('.');
    help_amount = amount['0'].split('').reverse();
    balance = help_amount['0'];
    for (let j = 1; j < help_amount.length; j++) {
      balance = help_amount[j] + balance;
      if ((j + 1) % 3 === 0) {
        balance = ' ' + balance;
      }
    }
    amount['0'] = balance;
    balance = amount.join(',');
    return account_balance = balance;
  }

  async getBlockInfo(block_num) {
    this.block_info = [];
    await this.getAssets();

    await Apis.instance().db_api().exec('get_block',  [block_num]).then(async (result) => {
      this.block_info = this.block_info.concat(result);
      for (let tr of this.block_info['0']['transactions']) {
        if ('amount' in tr['operations']['0']['1']) {
          for (let asset of this.list_assets) {
            if (tr['operations']['0']['1']['amount']['asset_id'] === asset['id']) {
              tr['operations']['0']['1']['amount']['asset_name'] = asset['name'];
              tr['operations']['0']['1']['amount']['amount'] = this.balance_refresh(tr['operations']['0']['1']['amount']['amount'], asset['precision']);
            }
          }
        }
      }
    });
  }
}
