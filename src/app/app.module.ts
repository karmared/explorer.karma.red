import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AccountsComponent } from './accounts/accounts.component';
import { AboutAccountComponent } from './about-account/about-account.component';
import { AccountObjectComponent } from './account-object/account-object.component';
import { BlockInformationComponent } from './block-information/block-information.component';
import { BalanceInformationComponent } from './balance-information/balance-information.component';
import { OperationInformationComponent } from './operation-information/operation-information.component';
import { FeesComponent } from './fees/fees.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { MarketsComponent } from './markets/markets.component';
import { AssetsComponent } from './assets/assets.component';
import { CommitteeComponent } from './committee/committee.component';
import { WitnessesComponent } from './witnesses/witnesses.component';
import { WorkersComponent } from './workers/workers.component';
import { ProxiesComponent } from './proxies/proxies.component';
import { VotingComponent } from './voting/voting.component';
import { SearchComponent } from './search/search.component';
import { WitnessInformationComponent } from './witness-information/witness-information.component';
import { StatisticInformationComponent } from './statistic-information/statistic-information.component';
import { HomePageComponent } from './home-page/home-page.component';

const routes = [
  {path: '', as: 'HomePage', component: HomePageComponent},
  {path: 'transactions', as: 'Transactions',  component: TransactionsComponent},
  {path: 'assets', as: 'Assets',  component: AssetsComponent},
  {path: 'markets', as: 'Markets',  component: MarketsComponent},
  {path: 'accounts', as: 'Accounts',  component: AccountsComponent},
  {path: 'search', as: 'Search',  component: SearchComponent},
  {path: 'fees', as: 'Fees',  component: FeesComponent},
  {path: 'witnesses', as: 'Witnesses',  component: WitnessesComponent},
  {path: 'committee', as: 'Committee',  component: CommitteeComponent},
  {path: 'accounts/:name', as: 'AboutAccount',  component: AboutAccountComponent},
  {path: 'objects/:id', as: 'AccountObject',  component: AccountObjectComponent},
  {path: 'operations/blocks/:num', as: 'BlockInformation',  component: BlockInformationComponent},
  {path: 'balance/:id', as: 'BalanceInformation',  component: BalanceInformationComponent},
  {path: 'operations/operation/:id', as: 'OperationInformation',  component: OperationInformationComponent},
  {path: 'operations/witness/:id', as: 'WitnessInformation',  component: WitnessInformationComponent},
  {path: 'operations/statistic/:id', as: 'StatisticInformation',  component: StatisticInformationComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    AboutAccountComponent,
    AccountObjectComponent,
    BlockInformationComponent,
    BalanceInformationComponent,
    OperationInformationComponent,
    FeesComponent,
    TransactionsComponent,
    MarketsComponent,
    AssetsComponent,
    CommitteeComponent,
    WitnessesComponent,
    WorkersComponent,
    ProxiesComponent,
    VotingComponent,
    SearchComponent,
    WitnessInformationComponent,
    StatisticInformationComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
